# Raspberry Pi Advertising App

## 1. Install Kivy on Raspberry Pi.

    sudo apt-get update
    sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev \
       pkg-config libgl1-mesa-dev libgles2-mesa-dev \
       python-setuptools libgstreamer1.0-dev git-core \
       gstreamer1.0-plugins-{bad,base,good,ugly} \
       gstreamer1.0-{omx,alsa} python-dev libmtdev-dev \
       xclip
    
    sudo pip install -U Cython==0.25.2
    
    sudo pip install kivy   

## 2. Install dependencies
    
    sudo pip install -r requirements.txt
    
  Set GPU memory size to 384 MByte with `sudo raspi-config` 
    
## 3. Launch application.
    
    python main.py