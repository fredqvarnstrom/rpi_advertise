import os
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.clock import Clock
from kivy.config import _is_rpi

Builder.load_file(os.path.join(os.path.dirname(__file__), 'menu.kv'))


class MenuScreen(Screen):

    event_adv = None

    def on_enter(self, *args):
        super(MenuScreen, self).on_enter(*args)
        if not _is_rpi:
            self.ids.video.volume = 0
            self.ids.video.source = 'video/wildlife.mp4'
        else:
            # self.ids.video.source = '/home/pi/Videos/bonusducks.mp4'
            self.ids.video.source = 'video/wildlife.mp4'

        self.ids.video.state = 'play'
        if self.event_adv is None:
            self.event_adv = Clock.schedule_interval(self.draw_adv, .05)

    def draw_adv(self, *args):
        if self.ids.announcement.right > 0:
            self.ids.announcement.x -= 10
        else:
            self.ids.announcement.x = self.width

    def on_leave(self, *args):
        super(MenuScreen, self).on_leave(*args)
        if self.event_adv:
            self.event_adv.cancel()
            self.event_adv = None
