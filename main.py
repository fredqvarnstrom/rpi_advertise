import os
os.environ['KIVY_BCM_DISPMANX_ID'] = '2'

from kivy.app import App
import kivy_config
from kivy.lang import Builder
from kivy import Logger
from kivy.base import ExceptionManager, ExceptionHandler
from kivy.uix.screenmanager import NoTransition
from screens.menu.screen import MenuScreen


class RPiAdvExceptionHandler(ExceptionHandler):
    def handle_exception(self, exception):
        Logger.exception('Exception: {}'.format(repr(exception)))
        if isinstance(exception, KeyboardInterrupt):
            return ExceptionManager.RAISE
        return ExceptionManager.PASS


# ExceptionManager.add_handler(RPiAdvExceptionHandler())


class RPiAdvApp(App):

    screen_names = []
    screens = {}

    def build(self):
        # TODO: Add more screens here.
        self.screen_names = ['menu']
        self.screens = {
            'menu': MenuScreen()
        }

        # Load root widget
        self.root = Builder.load_file(os.path.join(os.path.dirname(__file__), 'main.kv'))

        # Switch to the default screen.
        self.go_screen('menu')

    def go_screen(self, destination):
        """
        Switch the current screen of screen manager
        :param destination:
        :return:
        """
        sm = self.root.ids.sm
        try:
            sm.transition = NoTransition()
            sm.switch_to(self.screens[destination])
        except Exception as e:
            Logger.exception('Failed to move to {} - {}'.format(destination, e))

    def stop(self, *largs):
        super(RPiAdvApp, self).stop(*largs)
        os.kill(os.getpid(), 9)


if __name__ == '__main__':
    app = RPiAdvApp()
    try:
        app.run()
    finally:
        pass
